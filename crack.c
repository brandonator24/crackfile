#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=20;        // Maximum any password will be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a target hash, crack it. Return the matching
// password.
char * crackHash(char *target, char *dictionary)
{
    // Open the dictionary file
    FILE *in = fopen(dictionary, "r");
    if (in == NULL)
    {
        perror("can't open");
        exit(1);
    }

    // Loop through the dictionary file, one line
    // at a time.
    char *pw = malloc(sizeof(char)*PASS_LEN);
    while(fgets(pw,PASS_LEN,in) != NULL)
    {
        char *nl = strchr(pw, '\n');
        if (nl != NULL)
        {
            *nl = '\0';
        }
    

        // Hash each password. Compare to the target hash.
        // If they match, return the corresponding password.
        char *hash = md5(pw, strlen(pw));
        if(strcmp(hash, target) == 0)
        {
            return pw;
        }
        
        // Free up memory?
        free(hash);
    }
    fclose(in);
    return NULL;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Open the hash file for reading.
    FILE *hs = fopen(argv[1], "r");
    if (hs == NULL)
    {
        perror("can't open hashes");
        exit(1);
    }

    // For each hash, crack it by passing it to crackHash
    char target[HASH_LEN+1];
    while (fgets(target, HASH_LEN+1, hs) != NULL)
    {
        char *nl = strchr(target, '\n');
        if (nl != NULL)
        {
            *nl = '\0';
        }
        
        // Display the hash along with the cracked password:
        //   5d41402abc4b2a76b9719d911017c592 hello
        char *pw = crackHash(target, argv[2]);
        printf("%s %s\n", target, pw);
    }
    // Close the hash file
    fclose(hs);
    
    // Free up any malloc'd memory?
}
